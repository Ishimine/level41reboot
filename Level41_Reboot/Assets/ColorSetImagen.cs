﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class ColorSetImagen : MonoBehaviour {

    Image img;
    public Paleta.tags id = Paleta.tags.tXt_1;

    private void Awake()
    {
        img = GetComponent<Image>();

        Recursos.cambioColor += ActualizarColor;
        PedirColor();
    }

    void PedirColor()
    {
        if (Recursos.instance != null)
            ActualizarColor(Recursos.instance.setColorActual.GetPaleta());
    }



    void ActualizarColor(Color[] c)
    {
        img.color = c[(int)id];
    }

    private void OnDestroy()
    {
        Recursos.cambioColor -= ActualizarColor;
    }
}
