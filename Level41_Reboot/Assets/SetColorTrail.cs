﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetColorTrail : MonoBehaviour {


    TrailRenderer trail;
    Paleta.tags idColor = Paleta.tags.Jugador_1;



    private void Awake()
    {
        trail = GetComponent<TrailRenderer>();
        
        Recursos.cambioColor += ActualizarColor;
        PedirColor();
    }

    void PedirColor()
    {
        if (Recursos.instance != null)
            ActualizarColor(Recursos.instance.setColorActual.GetPaleta());
    }



    void ActualizarColor(Color[] c)
    {
        trail.startColor = c[(int)idColor];
    }

    private void OnDestroy()
    {
        Recursos.cambioColor -= ActualizarColor;
    }
}
