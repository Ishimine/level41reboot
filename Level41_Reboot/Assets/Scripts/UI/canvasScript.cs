﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class canvasScript : MonoBehaviour {

    public GameObject inGameUI;
    public GameObject menuPrincipal;
    
    public GameObject eventSys;

    static canvasScript instance;
    public bool enJuego;


    void Awake()
    {
        if (instance == null)
        {
            SelectorNivel.NivelCargado += NivelCargado;
            SceneManager.sceneLoaded += NivelCargado;
            instance = this;
            DontDestroyOnLoad(Camera.main.gameObject);
            DontDestroyOnLoad(this.gameObject);
            DontDestroyOnLoad(eventSys);
        }      
        else
        {
            Destroy(eventSys);
            Destroy(this.gameObject);
        }
    }
    
    private void NivelCargado()
    {
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            menuPrincipal.SetActive(true);
            inGameUI.SetActive(false);
            reiniciarMenuUI();
        }
        else
        {
            inGameUI.SetActive(true);
            menuPrincipal.SetActive(false);
            reiniciarInGameUI();
        }
    }

    void NivelCargado(Scene scene, LoadSceneMode mode)
    {
        if(scene.buildIndex == 0)
        {
            menuPrincipal.SetActive(true);
            inGameUI.SetActive(false);
            reiniciarMenuUI();
        }
        else
        {
            inGameUI.SetActive(true);
            menuPrincipal.SetActive(false);
            reiniciarInGameUI();
        }
    }


    void reiniciarMenuUI()
    {
        menuPrincipal.GetComponent<MenuPrincipalUI>().Reiniciar();
    }



    void reiniciarInGameUI()
    {
        inGameUI.GetComponent<InGameUI>().Reiniciar();
    }





}
