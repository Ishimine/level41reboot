﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class pantallaFinNivel : MonoBehaviour {



    DataDeNivel recordActual;
    DataDeNivel objetivoActual;

    public Text tiempo;
    public Text muertes;
    public Text barras;
    public Text tiempoRec;
    public Text muertesRec;
    public Text barrasRec;

    public Image iconoMuerte;
    public Image iconoBarras;
    public Image iconoTiempo;

    int m;
    int b;
    float t;

    static pantallaFinNivel instance;



    public void Awake()
    {
        if (instance == null)
        {
            instance = this;
            SelectorNivel.NivelCargado += Inicializar;
            GameController.pFinal = Activar;
            gameObject.SetActive(false);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void Inicializar()
    {
        GameController.pFinal = Activar;
        gameObject.SetActive(false);
    }


    void Activar()
    {
        if (!gameObject.activeSelf)
        {
            gameObject.SetActive(true);
            ActualizarContadores();
        }
    }

    public void ActualizarContadores()
    {
        t = GameController.getTiempo();
        tiempo.text = t.ToString("00:00.00");
        b = TouchControl.getBarrasCreadas();
        barras.text = b.ToString();
        m = CheckpointManager.getMuertes();
        muertes.text = m.ToString();
        ActualizarObjetivo();
        ActualizarRecord();

        CompararObjetivos();

    }

    void CompararObjetivos()
    {
        if(ObjetivoCumplido(m,objetivoActual.muertes))
        {
            muertes.color = Color.cyan;
            iconoMuerte.color = Color.cyan;
        }
        if (ObjetivoCumplido(b, objetivoActual.barras))
        {
            barras.color = Color.cyan;
            iconoBarras.color = Color.cyan;
        }
        if (ObjetivoCumplido(t, objetivoActual.tiempo))
        {
            tiempo.color = Color.cyan;
            iconoTiempo.color = Color.cyan;
        }
    }

    public void ActualizarObjetivo()
    {
        objetivoActual = ArbitroNiveles.getDataNivelObjetivos(SceneManager.GetActiveScene().buildIndex);
        tiempoRec.text = "/" + objetivoActual.tiempo.ToString("00:00.00");
        muertesRec.text = "/" + objetivoActual.muertes.ToString();
        barrasRec.text = "/" + objetivoActual.barras.ToString();
    }

    public void ActualizarRecord()
    {
        int nivelAct = SceneManager.GetActiveScene().buildIndex;
        if(nivelAct > 0)
            recordActual = ArbitroNiveles.getDataNivelRecords(nivelAct);
    } 


    public bool ObjetivoCumplido(int jugador, int objetivo)
    {
        return objetivo >= jugador;
    }
    public bool ObjetivoCumplido(float jugador, float objetivo)
    {
        return objetivo >= jugador;
    }


}
