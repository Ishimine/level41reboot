﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


public class ContadorUI : MonoBehaviour {

    Text txt;

    public void Restart()
    {
        txt.text = "00:00.00";
    }

	void Awake()
    {
        txt = GetComponent<Text>();
        FindObjectOfType<GameController>().actTiempo += ActualizarTexto;
	}
	
	
	void ActualizarTexto (float n)
    {
        if(txt != null)
            txt.text = n.ToString("00:00.00");
        //txt.text = n.ToString("00.00.00");
        //string timeText = string.Format("{0:D2}:{1:D2}:{2:D2}", n.Minutes, n.Seconds, n.Milliseconds);
    }
}
