﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CuentaRegresiva : MonoBehaviour {

    static CuentaRegresiva instance;


   public Text txt;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            txt = GetComponent<Text>();
            GameController.cuentaRegresiva = IniciarCuentaRegresiva;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }


    void IniciarCuentaRegresiva()
    {
        StartCoroutine(CuentaRegresivaInicio());
    }

    IEnumerator CuentaRegresivaInicio()
    {
        txt.text = "3";
        yield return new WaitForSecondsRealtime(1);
        txt.text = "2";
        yield return new WaitForSecondsRealtime(1);
        txt.text = "1";
        yield return new WaitForSecondsRealtime(1);
        txt.text = "";
    }


    
}
