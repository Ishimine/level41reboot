﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;


public class GameController : MonoBehaviour {


    public GameObject player;
    public GameObject meta;
    public Transform pPartida;
    public Transform pMeta;
    public static GameController instance;

    public static float tiempo;
    static public bool enPausa;
    static private bool inGame;

    TimeSpan timeS;


    public delegate void actFloat(float tiempo);
    public event actFloat actTiempo;


    public delegate void EstadoDeJuego(bool x);
    public static event EstadoDeJuego pausado;

    public delegate void Gatillo();
    public static event Gatillo finDeNivel;
    public static Gatillo pFinal;    
    public static Gatillo cuentaRegresiva;


    static bool cancelShowngTooltip;

    public static float getTiempo()
    {
        return tiempo;
    }

    void Awake ()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            pFinal = null;
            finDeNivel = null;
            DontDestroyOnLoad(this.gameObject);
            cuentaRegresiva = null;
            actTiempo = null;
            pausado = null;
            this.gameObject.SetActive(true);
            SceneManager.sceneLoaded += NivelCargado;
        }
    }
    
      

    void NivelCargado(Scene scene, LoadSceneMode mode)
    {
        if (scene.buildIndex == 0)
        {
            this.gameObject.SetActive(false);
        }
        else
        {
            pMeta = GameObject.FindGameObjectWithTag("pMeta").transform;
            pPartida = GameObject.FindGameObjectWithTag("pPartida").transform;

            if ((pMeta == null) || (pPartida == null))
            {
                Debug.LogError("pMeta/pPartida NO EXISTE");
                SelectorNivel.CargarNivel(0);
            }
            this.gameObject.SetActive(true);
            InicializarNivel();
        }

    }



        public static void IniciarJuego()
    {
        Time.timeScale = 1;
        inGame = true;
    }

    public static bool getInGame()
    {
        return inGame;
    }

    void Update()
    {
        if (inGame && !enPausa)
        {
            tiempo += Time.deltaTime;
            //timeS = TimeSpan.FromSeconds(tiempo);
            if (actTiempo != null) actTiempo(tiempo);
        }
    }

    IEnumerator CuentaRegresivaInicio()
    {
        yield return new WaitForSecondsRealtime(3);
        IniciarJuego();
        StopCoroutine("CuentaRegresivaInicio");
    }
       



    void CrearPartidaMeta()
    {
        GameObject p = Instantiate<GameObject>(player, pPartida);
        Instantiate<GameObject>(meta, pMeta);

        Camera.main.transform.position = p.transform.position;
        //Camera.main.GetComponent<SeguirObjetivo>().target = p.transform;
    }


    void InicializarNivel()
    {
        finDeNivel += IniciarContador;
        inGame = false;
        enPausa = false;
        tiempo = 0;
        CrearPartidaMeta();
        //Camera.main.GetComponent<SeguirObjetivo>().PosicionarCamara(pPartida.transform.position);

        Time.timeScale = 0;
        if (cuentaRegresiva != null)
            cuentaRegresiva();
        StartCoroutine("CuentaRegresivaInicio");
    }

    public static void ReiniciarNivel()
    {
        enPausa = true;
        Pausa();
        SelectorNivel.ReiniciarNivel();
    }

    public static void Pausa()
    {
        if (!enPausa)
        {
            enPausa = !enPausa;
            Time.timeScale = 0;
        }
        else
        {
            enPausa = !enPausa;
            Time.timeScale = 1;
        }

        if (pausado != null)
            pausado(enPausa);
    }

    public static void FinNivel()
    {
        inGame = false;
        if (finDeNivel != null)
            finDeNivel();
    }


    void IniciarContador()
    {
        StartCoroutine("CuentaRegresivaFinal");
    }
     

    IEnumerator CuentaRegresivaFinal()
    {
        yield return new WaitForSeconds(1f);
        if (pFinal != null)
            pFinal();
    }


    public static void CargarSiguienteNivel()
    {
        SelectorNivel.CargarNivel(SceneManager.GetActiveScene().buildIndex + 1);
    }


     float getTime()
    {
        return tiempo;
    }

}
