﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class ArbitroNiveles : MonoBehaviour {

    

    static ArbitroNiveles instance;




    public DataDeNivel[] recordsAux;
    public DataDeNivel[] objetivosAux;

    public static DataDeNivel[] records;
    public static DataDeNivel[] objetivos;




    public void Awake()
    {
        if(instance == null)
        {
            instance = this;
            CargarObjetivos();
            CargarRecords();
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public static DataDeNivel getDataNivelObjetivos(int i)
    {
        return objetivos[i-1];
    }
        

    public static DataDeNivel getDataNivelRecords(int i)
    { 
        return records[i-1];
    }

    public void CargarObjetivos()
    {
        CargarArreglo(ref objetivosAux, "objetivos");
    }

    public void CargarRecords()
    {
        CargarArreglo(ref recordsAux, "records");
    }    

    public void GuardarObjetivos()
    {
        GuardarArreglo(ref objetivosAux, "objetivos");
    }

    public void GuardarRecords()
    {
        GuardarArreglo(ref recordsAux, "records");
    }

    public static void CargarArreglo(ref DataDeNivel[] ar, string nombreFisico)
    {
        if (File.Exists(Application.persistentDataPath + "/" + nombreFisico))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/" + nombreFisico, FileMode.Open);
            ar = (DataDeNivel[])bf.Deserialize(file);
            file.Close();
        }
    }

    public static void GuardarArreglo(ref DataDeNivel[] ar, string nombreFisico)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/" + nombreFisico);
        bf.Serialize(file, ar);
        file.Close();
    }


    public void OnValidate()
    {
        ActRecords();
        ActObjetivos();
    }

    private void ActRecords()
    {
        records = recordsAux;
    }

    private void ActObjetivos()
    {
        objetivos = objetivosAux;
    }

}
