﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour {

    public int CheckID;
    public bool activado = false;



    public Vector3 direccion;



    void PuntoActivado()
    {
        //GAtillar animacion de activacio
        activado = true;
        ActivarColor();
    }

    void ActivarColor()
    {
        GetComponent<SpriteRenderer>().color = Color.cyan;
    }


    public void Update()
    {
        if(activado)
            transform.Rotate(direccion * Time.deltaTime);
    }


    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            CheckpointManager.ActualizarPunto(CheckID);
            PuntoActivado();
        }
    }

}
